package br.com.victorwads.arvorenatal.helper

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.view.inputmethod.InputMethodManager


class AcessibilityHelper {

    companion object {
        fun reqeustFocus(view: View) {
            hideKeyboard(view)
            view.isFocusable = true
            view.isFocusableInTouchMode = true
            Handler(Looper.getMainLooper()).post {
                view.requestFocus()
                view.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
            }
        }

        internal fun hideKeyboard(view: View) {
            val imm = view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        }
    }
}
