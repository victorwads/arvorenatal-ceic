package br.com.victorwads.arvorenatal.model

class Nome(
    val nome: String,
    val side: SIDE,
    val section: SECTION
) {

    enum class SIDE(internal val value: Int) {
        L1(0), L2(1), L3(2), L4(3), L5(4), L6(5), L7(6), L8(7)
    }

    enum class SECTION constructor(internal val number: String) {
        S1("1"), S2("2"), S3("3"), S4("4")
    }

    override fun toString(): String {
        return nome
    }
}
