package br.com.victorwads.arvorenatal.model

import android.support.annotation.DrawableRes

class LadoDraw(
    val side: Nome.SIDE,
    @DrawableRes val image: Int
)
