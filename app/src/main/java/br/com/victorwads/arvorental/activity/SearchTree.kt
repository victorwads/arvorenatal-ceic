package br.com.victorwads.arvorenatal.activity

import android.content.Intent
import android.graphics.drawable.TransitionDrawable
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import br.com.victorwads.arvorenatal.model.LadoDraw
import kotlinx.android.synthetic.main.content_pesquisa_arvore.*
import java.util.*
import android.widget.ArrayAdapter
import android.widget.TextView
import br.com.victorwads.arvorenatal.R
import br.com.victorwads.arvorenatal.dao.NomesArvore
import br.com.victorwads.arvorenatal.helper.AcessibilityHelper
import br.com.victorwads.arvorenatal.model.Nome
import kotlinx.android.synthetic.main.activity_pesquisa_arvore.*


class SearchTree : AppCompatActivity() {

    private val dao = NomesArvore()
    private val sides: List<LadoDraw> = Arrays.asList(
        LadoDraw(Nome.SIDE.L1, R.drawable.lado1),
        LadoDraw(Nome.SIDE.L2, R.drawable.lado2),
        LadoDraw(Nome.SIDE.L3, R.drawable.lado3),
        LadoDraw(Nome.SIDE.L4, R.drawable.lado4),
        LadoDraw(Nome.SIDE.L5, R.drawable.lado5),
        LadoDraw(Nome.SIDE.L6, R.drawable.lado6),
        LadoDraw(Nome.SIDE.L7, R.drawable.lado7),
        LadoDraw(Nome.SIDE.L8, R.drawable.lado8)
    )
    private var side = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pesquisa_arvore)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.app_title)

        bindViews()
        loadTree(side)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_principal, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_helphelp -> startActivity(Intent(this, HelpActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun bindViews() {
        btn_prev.setOnClickListener { prevSide() }
        btn_next.setOnClickListener { nextSide() }

        val adapter = ArrayAdapter<Nome>(this, android.R.layout.simple_dropdown_item_1line, dao.all)
        input_search.setAdapter(adapter)
        input_search.setOnItemClickListener { adapterView, view, i, l ->
            showNome(adapter.getItem(i))
        }
    }

    private fun showNome(item: Nome) {
        loadTree(item.side.value)
        loadSection(item.section)
    }

    private fun loadSection(section: Nome.SECTION?) {
        view_section1.visibility = View.INVISIBLE
        view_section2.visibility = View.INVISIBLE
        view_section3.visibility = View.INVISIBLE
        view_section4.visibility = View.INVISIBLE

        var sectionView: TextView? = null
        when (section) {
            Nome.SECTION.S1 -> sectionView = view_section1
            Nome.SECTION.S2 -> sectionView = view_section2
            Nome.SECTION.S3 -> sectionView = view_section3
            Nome.SECTION.S4 -> sectionView = view_section4
        }
        sectionView?.let {
            AcessibilityHelper.reqeustFocus(it)
            it.visibility = View.VISIBLE
            it.text = getString(R.string.found_sec, section?.number)
            it.contentDescription = getString(R.string.found_sec_acessible, getSideNumber(), section?.number)
        }
    }

    private fun overflowSides(side: Int) = side >= sides.size - 1

    private fun nextSide() {
        if (overflowSides(side))
            return
        loadSection(null)
        loadTree(++side)
    }


    private fun prevSide() {
        if (side <= 0)
            return
        loadSection(null)
        loadTree(--side)
    }

    private fun loadTree(@DrawableRes side: Int) {
        this.side = side
        if (image_trees.drawable == null) {
            image_trees.setImageResource(sides[side].image)
        } else {
            val td =
                TransitionDrawable(arrayOf(image_trees.drawable, ContextCompat.getDrawable(this, sides[side].image)))
            image_trees.setImageDrawable(td)
            td.startTransition(250)
        }
        label_tree_side_name.text = getString(R.string.lado_name, getSideNumber())
        label_tree_side_name.contentDescription = getString(R.string.lado_name_acessible, getSideNumber())
    }

    private fun getSideNumber() = (sides[side].side.value + 1).toString()

}
